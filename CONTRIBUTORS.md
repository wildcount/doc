# Wildcount :: Contributors

* Didier Donsez (Université Grenoble Alpes, GINP, Polytech and LIG CNRS)
* Georges Quénot (LIG CNRS)

## 2021
* Elisa Beaugrand (INFO5, Polytech Grenoble)
* Louis De Gaudenzi (INFO5, Polytech Grenoble)
* Alexis Rollin (INFO5, Polytech Grenoble)
* Tom Graugnard (INFO5, Polytech Grenoble)
* Baptiste Jolaine (IESE5, Polytech Grenoble)
* Benoît Barre (IESE5, Polytech Grenoble)
* Aurélien Reynaud (IESE5, Polytech Grenoble)
* Grégoire Carrot (IESE5, Polytech Grenoble)
* Ethan Malecot (INFO4, Polytech Grenoble)
* Teva Geitner (INFO4, Polytech Grenoble)
* Paul Lambert (INFO4, Polytech Grenoble)

## 2022
* Romain Pigret-Cadou (PHELMA 2A)
* Simon Pitois (ENSIMAG 2A)
* Thais Bourret (ENSIMAG 2A)
* Léo Cordier (ESISAR 5A)
