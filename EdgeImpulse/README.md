# Wildcount :: Tutorial Edge Impulse

Ce tutoriel à pour but d'expliciter l'utilisation du logiciel Edge Impulse disponible à l'adresse :https://www.edgeimpulse.com/

Ce site est notamment utilisé pour générer les réseaux de neurones utilisés dans le prototype 2022 de wildcount.

### Compte

Il est simple de se créer un compte sur EI, cependant un compte à été crée pour le projet Wildcount qui est propriétaire de tous les projets conçus sur EI.

    Username : wildcount
    Email : wildcountwild@gmail.com
    mdp : XXXXXXX

### Projets existants

<img src="images/existingProject.png" alt="illustration of trap case" width="600"/>

* wildcount_spresense : C'est le projet principal qui à été mis en place en 2022. Il contient 10 classes d'animaux + vide. 
* Wolves : Petit réseau servant à détecter si un loup est présent sur l'image ou non. Peut être optimisé plus. 
* pigeonCount : Comme wildcount mais pour détecter si des pigeons sont présents ou non sur l'image. Avait pour but d'être testé sur un balcon mais cela à été impossible à cause du PIR sensor trop sensible à la lumière.
* personDetection : Réseau binaire qui prend une image et détecte si une personne est dessus ou pas. Simple test, peu d'intérêt.
* objectClass : Réseau classifiant des images selon 4 classes : une boite en plastique, une boite en carton, un rouleau de fil et vide. Aussi un projet de test. 

### Créer un projet 

Après avoir cliqué sur **Create new project**, entrez un nom de projet et sélectionnez l'option **Developer**.

Plusieurs options sont proposées. A priori dans le cadre du projet Wildcount nous faisons de la détection d'images, donc on utilise l'option **Images** puis l'option **Classify a single object (image classification)** et ensuite sélectionnez **Let's get started**

Il faut ensuite sélectionner le coeur sur lequel le réseau de neurone tournera. La spresense est présente sur EI : 

<img src="images/selectionCortex.png" alt="illustration of trap case" width="600"/>

### Ajouter des images

Dans le menu **Data acquisition**, cliquez sur **Let's collect some data**


<img src="images/dataacquisition.png" alt="illustration of trap case" width="600"/>

Et selectionner **Upload data** dans les options proposées. 

<img src="images/datauploader.png" alt="illustration of trap case" width="600"/>

Pour upload des images il y a plusieurs possibilités. D'abord vous pouvez upload chaque classe une par une en sélectionnant toutes les images d'une classe puis en choisissant **Enter label** et en précisant le label de ces images.
Sinon si les images sont bien nomées, vous pouvez choisir l'option **Infer from filename** et importer toutes les images d'un bloc. 


De plus, vous pouvez choisir de laisser EI split les images en Training/set automatiquement (aléatoirement et selon un ration de 80/20) ou bien choisir directement quelles images vont en train et en test. 

### Créer le modèle

Dans l'onglet **Impulse Design - Create Impulse**, sélectionnez la taille des images traitées par le réseau, la manière de modifier la taille des images (crop ou resize).

Sélectionnez le bloc **Image** en deuxième et le bloc **Transfert Learning (images)** en troisième.

Si tout est bien organisé, vous devez avoir vos classes écrites dans **Output features**

<img src="images/impulse.png" alt="illustration of trap case" width="600"/>

### Générer les images adaptées 

Dans l'onglet **Impulse Design - Image** cliquez sur l'onglet **Generate Features**  puis sur le bouton du même nom. Cela prendra un peu de temps en fonction du nombre d'image. C'est ici qu'elles sont modifiées en taille pour correspondre à la taille demandée par votre modèle.

<img src="images/imagesgeneration.png" alt="illustration of trap case" width="600"/>

### Entrainer le modèle

Dans l'onglet **Impulse Design - Transfert Learning** vous avez accès aux options pour entrainer le modèle. 

* **Number of training cycles** : Plus communément appellées periodes, c'est le nombre de cycle d'entrainement
* **Learning rate** : modifie le learning rate de votre apprentissage. Un learning rate plus faible ralentit l'apprentissage mais permet d'éviter l'overfitting.
* **Validation set size** : Pourcentage des images du dataset de train gardée pour la validation de l'entrainement.
* **Auto-balance dataset** : Demande à EI de modifier la répartition en ajoutant de l'augmentation de données pour certaines classes en particulier et en modifiant le poids de chaque classes, notamment pour celles en sous-nombre. Très utile pour le projet wildcount vu les écarts de taille pour chaque classe. 
* **Data Augmentation** : Permet d'augmenter artificiellement le nombre d'images en appliquant des transformations sur celles existantes comme de la rotation, du changement de couleur... Permet de réduire l'overfitting. 
* **Choose a different model** : Permet de choisir le modèle utilisé pour le transfert learning. Le plus efficace étant MobileNetV2.
Il est aussi possible de choisir le nombre de neurones de l'avant dernière couche ainsi que le taux de dropout. (A regarder dans la documentation pour voir le fonctionnement exact).

Lorsque tous les paramètres sont bien réglés, sélectionnez **Start training**.

Après un temps plus ou moins long, vous devriez voir apparaitre les résultats de l'entrainement a droite avec des informations utiles comme la matrice de confusion, l'accuracy, le temps d'inférence, le coût en RAM et en flash...

<img src="images/transfert.png" alt="illustration of trap case" width="1200"/>

Vous pouvez aussi accéder au code brut directement en cliquant sur les trois points en haut a droite et en sélectionnant **Switch to Keras (expert) Mode** afin de modifier plus finement le modèle. 

### Déploiement

Dans l'option **Deployment** Vous pouvez sélectionner la librairy ou le firmware pour lequel vous voulez déployer votre réseau de neurone. Nous avons utiliser les libraires arduino. Vous pouvez aussi analyser les optimisations avant de build votre libraire. 

### Autres onglets 

* L'onglet **EON Tuner** est un très bon outil qui permet de tester assez rapidement un grand nombre de modèles différents afin d'avoir une bonne base sur lequel commencer son travail. Cliquez sur **Start EON tuner**.
Lorsque c'est terminé, vous pouvez importer les paramètres directement en cliquant sur le bouton **Select** du modèle que vous voulez. Les paramètres seront alors modifiés dans **Impulse design - Transfert Learning**. Attention les anciens paramètres seront effacés. 

* **Versionning* : permet de sauvegarder une version du modèle afin de pouvoir y revenir plus tard sans devoir refaire un projet. 

