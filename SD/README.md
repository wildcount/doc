# Ecriture sur carte SD
Utilise le protocole spi, plus d’informations [ici](lec12_sd_card.pdf)

Format des commandes SD spi :

![](img/SD_commands_format.png)

![](img/SD_commands.png)


![](img/SD_flowchart.png)