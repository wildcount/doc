# Rapport final 2022 - WildCount

**Stage Assistant ingénieur Ensimag**

Ce rapport présente l'état du projet, les réalisations et les conditions d'utilisation du prototype Wildcount réalisé par  Romain Pigret-Cadou, Simon Pitois et Thaïs Bourret. 

![Photo d'un boîtier WildCount](./images/wildcount2022_beautifulp.jpg)

*Figure 1 : Photo d'un boîtier WildCount*


## Sommaire 

- [Hardware stuff](#hardware)
    - [Présentation](#présentation)
    - [PIR Sensor](#pir-sensor)
- [Software stuff](#software)  
    - [Présentation générale](#présentation-générale)
    - [Carte Sony](#carte-sony)
    - [Batterie](#batterie)
    - [PIR Sensor](#pir-sensor-1)
    - [Lora & RTC](#lora--rtc)
    - [Cold sleep](#cold-sleep)
    - [Watchdog](#watchdog)
    - [Modèle](#modèle)

## Hardware 

![Photo de l'intérieur d'un boitier wildcount](./images/wildcount2022_openproto1.jpg)
*Figure 2 : Photo de l'intérieur du boitier*

### Présentation 
Au Fablab, nous avons imaginé et réalisé ce boitier comprenant tous les éléments nécessaires au bon fonctionnement du prototype. Cela contient : Une carte Sony Spresense avec Caméra et lecteur SD embarqués, rm2483 (Lora), Batterie, PIR sensor (*2). 

Tous les branchements sont effectués par wrappage et la boite est étanchéifiée grâce à la colle néoprène. Le tout est fixé sur une plaque de bois par des vis. Démontable assez facilement. 

### PIR sensor 
Nous avons d'abord utilisé les PIR gros (en haut à gauche sur le prototype). Mais leur efficacité était très faible. Nous avons donc emprunté des PIR sensors au Fablab bien plus performants. D'où la présence de deux PIR sur le prototype. 


## Software

### Présentation générale

La partie Software du prototype à été traitée sur Arduino. Pour utiliser la Spresense sur cette plateforme, il faut ajouter le package Spresense dans les préférences Arduino : https://github.com/sonydevworld/spresense-arduino-compatible/releases/download/generic/package_spresense_index.json 

Le code est composé d'un programme principale [wildcount.ino](https://gitlab.com/wildcountfirmware-spresense-arduino/-/blob/master/wildcount/wildcount.ino), de fichiers de configuration [config.h](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/wildcount/src/config.h) pour les définitions des variables globales et [global.h](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/wildcount/src/global.h), et de fichiers sources pour chaques composants du prototype : [fichiers sources](https://gitlab.com/wildcount/firmware-spresense-arduino/-/tree/master/wildcount/src/components) et les fichiers sources pour le payload lora [payload](https://gitlab.com/wildcount/firmware-spresense-arduino/-/tree/master/wildcount/src/payload_2022)

### Carte Sony

Pour notre prototype, nous utilisons la Spresense avec sa caméra intégrée et le lecteur de carte SD.

![Photo de la caméra de la Spresense](./images/wildcount2022_camera.jpg)
*Figure 3 : Photo de la caméra de la Spresense*

![Photo du lecteur SD de la Spresense](./images/wildcount2022_sd.jpg)
*Figure 4 : Photo du lecteur SD de la Spresense*

#### Caméra 

La caméra est gérée par la librairie Sony [Camera.h](https://developer.sony.com/develop/spresense/developer-tools/api-reference/api-references-arduino/Camera_8h.html)

La caméra ne peut prendre des photos seulement en 4 formats : YUV422, RGB565, GRAY, JPEG. Le format JPEG est indépendant et sert seulement au stockage des photos. Car les conversions se font seulement du format YUV aux formats RGB565 et GRAY. Dans le prototype, nous prenons donc les photos au format YUV422 que nous enregistrons en brut dans la carte SD avant de les transformer en RGB888 pour le modèle. 

Il faut ensuite utiliser le [convertisseur sd](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/tools/convert_sd_pictures.py) avec la commande :
```shell
python3 convert_sd_pictures.py path_de_la_carte_sd
```

L'autre problème concerne les changement de taille d'image. La librairie Sony, avec la fonction :
```
clipAndResizeImageByHW()
```

oblige que les changements de taille soit de la forme 2^k. 


#### Carte SD

La carte SD est gérée par la librairie Sony [SDHCI.h](https://developer.sony.com/develop/spresense/developer-tools/api-reference/api-references-arduino/SDHCI_8h.html)

La librairie SDHCI.h gert la carte SD formatée en FAT32. On enregistre les images dessus avec le nom *date.txt* et le fichier [convertisseur sd](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/tools/convert_sd_pictures.py) transforme ce .txt en un fichier *classeReconnue_date.bmp* 

[camera.cpp](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/wildcount/src/components/camera.cpp)

### Batterie

Le boitier contient deux emplacements de batterie (la grosse batterie et la plus petite en bleue). 

### Pir Sensor

Le PIR sensor n'a pas de librairie pour lui. Il active seulement le GPIO sur lequel il se trouve, réveillant la carte lorsqu'un mouvement est détecté. 

### Lora & RTC

Nous utilisons le module rn2483 pour transférer le signal LORA : [rn2483.h](https://gitlab.com/wildcount/firmware-spresense-arduino/-/blob/master/wildcount/src/components/rn2483.cpp) 

Nous avons déclaré deux variables de temps de durée différentes. Par défaut une de 15 minutes et une de 4 heures. 
Nous envoyons par Lora toutes les 15 minutes les compteurs relatifs (c'est à dire le nombre d'animaux comptés depuis le dernier envois). 
Tous les jours nous envoyons les compteurs absolus ainsi que la MAJ de la RTC et les infos.  

On peut gérer la file d'attente en cas de requête du serveur mais on n'envoit pas de réponses (TODO). 


### Cold sleep 

Lorsque le programe ne fait rien, on l'endort en cold sleep. Dans la carte il y a deux sleep, Cold et Deep. Le deep endort pour une durée imposée par la RTC sans pouvoir réveille autrement alors que le cold sleep permet le réveil par une interruption (c'est donc ce qu'on utilise).

### Watchdog

Nous utilisons le Watchdog proposé par la Spresense dans la librairy [Watchdog.h](https://developer.sony.com/develop/spresense/developer-tools/api-reference/api-references-arduino/Watchdog_8h.html).

Lorsque le programme se réveille, on lance un watchdog d'une durée égale à un temps minimum (temps d'attente dans le programme) plus une minute. Si le programe plante à cause du Watchdog alors on le note dans les infos envoyés chaque jour. 

### Modèle

Le plus gros problème du prototype est situé sur le modèle. 

Avec Edge impulse, nous avons développé plusieurs modèle, un pour les animaux de wildcount, un pour des objets simples, un pour savoir si il y avais quelqu'un sur la photo...

Mais le problème est toujours le même, sur la carte, on ne peux pas mettre des images de tailles plus grandes que 160x160 voire 80x80 car avec tous les autres composants, la taille maximale est atteinte. 

Or sur des images de cette taille, l'accuracy du modèle est très faible. Dans l'idéal il faudrait 4 à 8 GO de RAM comme pour la Maix pour avoir plus de place et pouvoir traiter un modèle avec des images de tailles 224*224 (idéal pour mobilenet). 

On a de plus un problème de temps de compilation pour le prototype avec le modèle. On a donc développé les autres modules sur la branche develop-easy pour pouvoir compiler rapidement.

Il y a aussi le problème du nombre d'image entrainant les modèles. Avec des classes surreprésentées et d'autres ultra minoritaires, on a de grosses difficultés à trouver certaines classes. 



