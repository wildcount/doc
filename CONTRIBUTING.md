# Contributing rules

This document summarizes all the rules in place for this project.

## Choice of the functionalities to be implemented and attribution to a developer

We manage the project via the GitLab manager in the "Board" tab. Each task corresponds to an issue and for each issue a corresponding branch must be created. Branch names and commits must be in English. Each commit must contain the issue number in the message.
Example of issue: "Add a GPS functionality", issue #1
Example of a branch name: PaymentFromRegistration ( Camel Case format)
Example of commit message: git commit -m "GPS functionality added #1"

## Implementation of the functionality

For each important new feature, it will be necessary to also implement the corresponding tests or to make an issue for it to be done.

## Provision of the work carried out

At the end of the work session, a merge request must be done, so that the code can be reviewed by a team member. Thanks to a protection rule, the merge can't be done if the code has not been reviewed by at least one person.

## Validation and integration (or not) of work

Once the code review is done, and if the work is approved, we integrate the work to the "master" branch and delete the work branch. The developer of the feature is then in charge of checking if his work integrates well with the rest. If it is not the case, he must open a way out and fix the bug on a new branch. If the work does not fit after the code review, the developer improves his work on the branch created initially. If there are conflicts at the time of merge it is up to the developer to resolve them.
