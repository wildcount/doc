# Rapport final - WildCount

**Projet de fin d'études collaboratif avec des étudiants de IESE**

Réalisé par Alexis ROLLIN, Élisa BEAUGRAND, Louis DE GAUDENZI, Tom GRAUGNARD, Baptiste JOLAINE, Benoît BARRE, Grégoire CARROT, Aurélien REYNAUD

![Photo d'un boîtier WildCount](./images/rapport_final01.png)

*Figure 1 : Photo d'un boîtier WildCount*

## Rappel du sujet/besoin et cahier des charges

Les gardes forestiers et autres métiers de la protection animale utilisent des pièges photos dans les parcs naturels pour recenser les espèces présentes dans la région. Le problème des pièges actuels est que sans connexion internet suffisante, il est obligatoire d’aller envoyer une personne pour récupérer les photos prises et stockées sur une carte SD, à des intervalles de temps parfois longs (six mois à un an). Il est malheureusement possible que les pièges photos et la carte SD soient dégradés par les intempéries ou les animaux, et donc que beaucoup de données soient perdues.

C’est ici qu’intervient WildCount afin de minimiser la perte de données. En intégrant une connexion longue distance et un réseau de neurones au piège photo, l’appareil peut détecter automatiquement les espèces animales qui sont aperçues, et envoyer ces informations en direct sur des serveurs. Les photos en elle-même devront quand même être récupérées à la main, car trop lourdes pour être envoyées sur le réseau basse consommation. 

## Technologies employées

La majeure partie de notre projet a été de créer un réseau de neurones capable de reconnaître les animaux de la forêt. Pour ce faire, nous avons utilisé Python avec la bibliothèque TensorFlow pour se servir de MobileNet. Au vu de la puissance de nos ordinateurs personnels, nous avons utilisé l’outil Grid5000 pour avoir la puissance de calcul nécessaire pour entraîner nos réseaux de neurones. Afin d’obtenir une interface graphique sur un serveur distant, nous avons utilisé les carnets Jupyter.

La carte embarquée GreenWaves Technologies est programmable en C, et communique avec les serveurs via un réseau LoRaWAN. Node-RED sert à faire le lien entre LoRaWAN et la base de données InfluxDB, et Grafana permet d’afficher ces données sur un tableau de bord sous forme de graphiques.
Pour communiquer entre la carte et le serveur via LoRa, nous avons eu besoin de créer des encodeurs/décodeurs en C pour la partie embarquée et en JavaScript pour le cloud.

## Architecture technique

Nous proposons un piège photo capable de reconnaître les animaux et de transmettre l’information de leur détection en temps réel. Pour l’utiliser, il suffit de fixer le boîtier sur un arbre, puis de consulter le tableau de bord depuis un navigateur internet. 

Le déroulement est le suivant : tout d’abord, un animal passe devant le piège. Celui-ci se déclenche et prend une photo. Puis, une IA embarquée dans le piège reconnaît l’animal photographié. Cette information est ensuite transmise en temps-réel via un réseau à faible consommation. L’information est reçue par le serveur et s’affiche sur le tableau de bord, sur lequel les statistiques sur le nombre d’animaux comptés et leurs espèces pourront être consultées. Le temps-réel permet également de savoir instantanément si l’appareil a un problème, comme une dégradation matérielle ou un niveau de batterie faible.

![Schéma de l’architecture du projet WildCount](./images/rapport_final02.png)

*Figure 2 : Schéma de l’architecture du projet WildCount*

Voici un aperçu des premières et dernières couches de notre modèle final. Notons que les couches Mul et Sub empêchent le chargement du modèle dans la GapPoc-A, comme nous le détaillons dans la partie suivante.

![Première couche du modèle final](./images/rapport_final03.png)

*Figure 3 : Première couche du modèle final*

![Dernière couche du modèle final](./images/rapport_final04.png)

*Figure 4 : Dernière couche du modèle final*

## Réalisations techniques

Voici les principales tâches effectuées afin de mener ce projet à bout.

### Scraping

Afin de pouvoir entraîner le réseau de neurones à reconnaître des animaux, nous avions besoin de beaucoup de photos de ces mêmes animaux. Pour récupérer toutes ces données, nous avons écrit des scripts pour télécharger des images sur différents sites, tels que Google, Bing, ou Zooniverse. Nous avons aussi utilisé les 1800 images fournis par le Parc national des Écrins pour alimenter notre banque d’images.

![Photo de chamois provenant du parc national des Écrins Valbonnais](./images/rapport_final05.png)

*Figure 5 : Photo de chamois provenant du parc national des Écrins Valbonnais*

Une fois un nombre suffisant d’images récupérées, il faut ensuite les classifier en fonction de ce que l’on cherche à reconnaître. Nous avons récupéré environ 8200 photos que nous avons classifiées en 24 catégories :

* blaireau
* bouquetin
* brebis
* cerf_ou_biche
* chamois
* chauve_souris
* chevre
* chevreuil
* chien
* cycliste
* ecureuil
* hermine
* lievre_europe
* lievre_variable
* loup
* marmotte
* martre
* mouflon
* oiseau
* promeneur
* renard
* rien
* sanglier
* vache

L’algorithme d'entraînement du réseau de neurones va ensuite se servir de ces images pour entraîner le réseau de neurones à faire la différence entre les photos que le boîtier prendra. Le réseau de neurones pourra ainsi reconnaître les animaux présents sur l’image.

### Réseau de neurones

Une des parties majeures du projet a été de créer un réseau de neurones spécialisé dans la détection et la reconnaissance d’espèces animales présentes dans le parc national des Écrins ainsi que les randonneurs et cyclistes. Pour ce faire, nous avons utilisé la librairie TensorFlow (version 1.15) afin de récupérer un modèle MobileNet v2 et de le réentraîner avec nos jeux d’images, c’est-à-dire les photos fournies par le Valbonnais et les images scrapées précédemment.

Après un premier jet sur nos machines, il apparaissait que nos ordinateurs personnels n’étant pas suffisamment puissants pour entraîner un réseau de neurones, nous avons alors demandé à utiliser l’outil Grid5000. Celui-ci nous a permis d’obtenir la puissance de calcul nécessaire à la création non seulement de modèles de test, mais également du modèle final.

Notre programme de réentraînement est une adaptation du script `retrain.py` fourni sur le dépôt Github de Tensorflow. Initialement prévu pour les modèles de type Inception, il a dû être adapté pour les modèles MobileNet. Les images d’entraînement étant en couleur et les images de la caméra en noir et blanc, une transformation des images d’entraînement en nuances de gris a été ajoutée, sachant qu’elles subissaient déjà un redimensionnement en 224×224. Si l’on garde un œil critique sur ce qui est fait, ces altérations successives sont nécessaires mais provoquent une perte d’information non négligeable qui oblige à avoir une quantité massive d’images pour entraîner le réseau correctement, et l’on peut également se questionner sur les images scrappées qui, bien que contenant des animaux, sont souvent trop propres pour correspondre à la réalité terrain (animal bien éclairé, au centre de l’image, non dissimulé par la végétation). Notre dataset actuel contient beaucoup plus d’images scrappées que d’images du Valbonnais, un entraînement optimal aurait évidemment pour dataset une majorité d’images du Valbonnais.

Ceci dit, la mise en place du réseau de neurones n’est pas la partie dans laquelle nous avons rencontré le plus de problème. La carte n’accepte que des modèles au format tflite, une exportation de notre modèle est nécessaire. Cependant, établir un fichier tflite compatible avec la GapPoc-A n’est pas chose aisée. Tout d’abord, nous nous sommes rendus compte que la carte n’accepterait que des modèles optimisés, en l'occurrence quantifiés avec des entiers sur 8 bits. Le convertisseur Tensorflow Lite propose des options pour paramétrer la quantification, mais il n’est capable de quantifier que les opérations les plus basiques, et notre modèle MobileNet v2 contenait des opérations pour lesquelles aucune fonction de quantification n’avait encore été implémentée. Nous avons donc changé de version de MobileNet et nous nous sommes basés sur un modèle MobileNet v1. Une fois notre modèle exporté en fichier tflite avec quantification en entiers 8 bits, nous avons essayé de le charger sur la GapPoc-A mais des couches du modèle, pourtant extrêmement basiques, ne semblaient pas autorisées. Nous avons contacté GreenWaves via une issue Github, mais nous avons obtenu des réponses qui étaient néanmoins complexes à mettre en place dans le temps restant.

Nous avons donc un tflite fonctionnel et optimisé capable de reconnaître 24 classes différentes, mais encore incompatible avec la GapPoc-A.

### Payload

La payload représente les messages envoyés entre le boîtier et le serveur WildCount, qui passeront à travers le réseau LoRaWAN. Le circuit est composé d'encodeurs et de décodeurs qui convertiront les données dans un format défini, afin qu’elles soient envoyées ou stockées correctement. 

Pour communiquer, nous disposons de deux grands types de messages : les messages UP en provenance de la carte, et les messages DOWN en provenance du serveur. Nous avons donc eu besoin d’écrire, côté serveur, un décodeur de messages UP et un encodeur de messages DOWN en Javascript. À l’inverse, un décodeur de messages DOWN et un encodeur de messages UP ont été écrits en C du côté de la partie embarquée.

Nous avons évidemment rencontré des problèmes lors de la réalisation de ces programmes. Tout d’abord, il a fallu comprendre ce qui était demandé, ainsi que la différence entre les messages UP et DOWN. Il a également fallu se coordonner avec les IESE, car ce sont eux qui s'occupent de l'encodage et le décodage du côté de la carte embarquée. Les 2 parties étant censées communiquer, il faut donc que l’on soit sûr que les encodages et décodages soient équivalents. Il a également fallu faire attention aux notions de boutisme, car il est possible que la carte embarquée et les ordinateurs n’utilisent pas la même convention (little endian/big endian).

## Gestion de projet

### Réunions

Tout au long du projet, nous avons essayé d’appliquer la méthode Agile SCRUM. Pour ce faire, nous avons entre autres réalisé des daily meetings avec les huit membres du groupe, afin de se tenir au courant de l’avancement de chacun dans leurs tâches à réaliser.

Ces réunions sont donc des réunions en plus de celles que l’on a effectuées avec les porteurs du projet, au cours desquelles nous avons pu poser nos questions, ainsi que de tenir les porteurs au courant de l'avancée du projet pendant la semaine passée.
Pour les préparer, nous disposions d’un Journal de Bord, dans lequel nous définissions un ordre du jour avant chaque réunion. Cela permettait de ne pas oublier les points à aborder, ainsi que de garder les réunions courtes en ne dérivant pas sur des sujets qui n’étaient pas prévus.

### Tâches GitLab

Les tâches GitLab ont une double utilité. En plus de permettre à tous les collaborateurs de voir l’avancement du projet, elles permettent aussi de se séparer le travail facilement. Nous avons créé des tags à affecter à ces tâches, pour savoir leur avancement et à qui elles étaient destinées. Elles nous ont également permis de tenir un tableau de bord de l’avancement.

### Planning

![Diagramme de Gantt réel](./images/rapport_final06.png)

*Figure 6 : Diagramme de Gantt réel*

Voici le planning effectif de notre projet, avec les tâches réalisées et les dates correspondantes. Vu que l’on était 4 pour la partie INFO, on a pu séparer les différentes tâches, ce qui a permis de paralléliser le travail.

![Diagramme de Gantt prévisionnel](./images/rapport_final07.png)

*Figure 7 : Diagramme de Gantt prévisionnel*

Ci-dessus se trouve le diagramme de Gantt prévisionnel. On y trouve des tâches qui ont disparu par la suite, comme celle de Grafana par exemple, que nous avons jugée comme moins prioritaire que les autres. Il y a aussi des tâches que nous n’avions pas prévu, comme le fait de devoir travailler sur Grid5000 et donc de devoir prendre en main l’outil.

### Rôles des membres

Pour assurer une gestion efficace du projet, nous avons défini des rôles pour chaque membre du groupe. Déjà, chaque collaborateur est bien évidemment développeur. Élisa Beaugrand est cheffe de projet, c’est elle qui gère le projet du côté INFO. Pour s’assurer du bon déroulement des Daily Meeting, nous avons assigné Alexis Rollin au rôle de Scrum Master. 

### Outils

Nous nous sommes servis de plusieurs outils afin de pouvoir collaborer dans les meilleures conditions. Nous disposions d’un Google Drive afin de partager des documents lourds, notamment les photos d'animaux prises dans le parc national des Écrins. 

La totalité des images utilisées pour l'entraînement se trouve dans le répertoire irim de Grid’5000. Grid’5000 nous a permis de travailler en collaboration sur le réseau de neurones tout en bénéficiant d’une puissance de calcul bien supérieure à celle de nos machines.

Le code, produit par les IESE comme par les INFO, est disponible et accessible par tous les membres du groupe sur un dépôt GitLab. L’outil nous a également été utile pour la gestion de projet, grâce aux tickets qui permettaient à tout le monde de constater l’avancement du projet et quelles tâches étaient libres, en cours, etc. GitLab permet également de trier et visualiser ses tickets sous la forme d’un Kanban.

Pour parler de vive voix avec les collaborateurs en distanciel, nous avons deux outils à notre disposition :

* Discord pour parler entre membres du projet
* Zoom pour les réunions avec les porteurs

## Métriques logicielles
Nous avons compté les lignes de code écrites dans la partie INFO. Elles sont réparties comme ceci :

* `backend` : ~1000 lignes
* `firmware` : 340 lignes
* `model` : 318 lignes

Cela revient à un total d'environ 1650 lignes de code. À partir de ce nombre de lignes, nous avons utilisé l’outil COCOMO de la NASA pour calculer les métriques suivantes :

![Résultats du COCOMO](./images/rapport_final08.png)

*Figure 8 : Résultats du COCOMO*

On remarque que l’on retrouve à peu près les durées que l’on a eu. En effet, 5 mois d’effort à 4 revient à 1,25 mois de travail total, ce qui est globalement le temps de projet qui nous a été alloué.

Nous avons utilisé différents langages pour effectuer ce projet :

* Le langage Python a été utilisé pour définir et entraîner le réseau de neurones.
* Tout le code concernant la carte a été écrit en C, et notamment l’encodeur et le décodeur qui permettent de gérer le payload du côté du boîtier.
* Situé à l’autre bout de la chaîne, l’encodeur et le décodeur du serveur ont été écrits en JavaScript.

## Conclusion

Ce projet a été une bonne expérience pour tous les membres, que ce soit d’un point de vue technique, relationnel, ou encore en termes de gestion de projet. 

Tout d’abord, le sujet est un bon mélange de réseau (LoRa) et de multimédia (Réseau de neurones), ce qui permet d’être polyvalent. L’intelligence artificielle est un sujet que l’on a beaucoup vu dans les cours théoriques, mais peu pratiqué réellement. C’est donc une expérience intéressante à avoir, notamment pour ceux qui voudraient travailler dans ce domaine à l’issue de leurs études. Bien que tous les étudiants en INFO soient en spécialité multimédia, la partie réseau n’a pas été trop compliquée avec l’aide des étudiants en IESE qui connaissaient bien la technologie utilisée. Cela nous a permis de nous remettre à niveau dans un domaine où nous ne sommes pas proficient.

Concernant l’aspect relationnel du projet, nous avons travaillé pour la première fois avec un groupe ne provenant pas de notre filière, et ainsi n’étant pas dans notre domaine. Nous avons rencontré quelques difficultés au départ, mais nous avons rapidement su nous coordonner afin de produire un travail efficace.

Enfin, la gestion de projet, bien qu’un domaine dans lequel nous avons accumulé de l’expérience, resta un challenge tout au long de ce projet. En effet, nous avons travaillé avec les IESE, qui ont beaucoup moins d'expérience que nous sur Git, la méthode Agile, … Il nous a donc fallu consacrer du temps à les former et leur faire comprendre l'intérêt de ses outils de collaboration. C’est donc la différence d’expertise dans les méthodes de gestion de projet qui créa des difficultés, chose qui n’a pas eu lieu dans les précédents projets avec tous les membres dans la filière INFO.

## Glossaire

### LoRaWAN

Acronyme de Long Range Wide Area Network. Réseau basse consommation à grande portée. Utile pour notre projet car on peut laisser un piège photo des mois sans recharger la batterie, au milieu de la montagne. 

### Réseau de neurones

Un type d’intelligence artificielle qui se base sur des couches de neurones. Les programmes s'entraînent sur des données afin de reconnaître des classes, et essayent ensuite de deviner à quelle classe appartient des nouvelles données. 

### Scraping

Le fait de récupérer des données sur internet qui serviront à entraîner le réseau de neurones.

### Payload

En français : charge utile. Signifie les données qui ont une signification dans un message. Ici plus généralement, désigne la partie qui sera encodée et décodée dans un message. 

## Bibliographie

TensorFlow Hub : https://tfhub.dev/google/imagenet/mobilenet_v1_100_224/classification/3

Convertisseur TensorFlow Lite : https://www.tensorflow.org/lite/convert
