// Utilisation d'un module RTC avec un Arduino Uno
// https://tutoduino.fr/
// Copyleft 2020
 
#include "RTClib.h"
 
RTC_DS1307 rtc;
 
void setup () {
  Serial.begin(9600);
 
  // Attente de la connection serie avec l'Arduino
  while (!Serial);
 
  // Lance le communication I2C avec le module RTC et 
  // attend que la connection soit operationelle
  while (! rtc.begin()) {
    Serial.println("Attente du module RTC...");
    delay(1000);
  }
 
  // Mise a jour de l'horloge du module RTC si elle n'a pas
  // ete reglee au prealable
  if (! rtc.isrunning()) {
    // La date et l'heure de la compilation de ce croquis 
    // est utilisee pour mettre a jour l'horloge
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
   
    Serial.println("Horloge du module RTC mise a jour");
  }
}
 
void loop () {
    DateTime now = rtc.now();
    char heure[15];
 
    // Affiche l'heure courante retournee par le module RTC
    // Note : le %02d permet d'afficher les chiffres sur 2 digits (01, 02, ....)
    sprintf(heure, "Il est %02d:%02d:%02d", now.hour(), now.minute(), now.second());
    Serial.println(heure);
     
    delay(5000);
}
