# WildCount

Inexpensive hunting camera for recognizing and counting the presence of humans (anonymous) and animals into wild and protected areas.

## Description

Remote detection of humans or large animals in natural areas is an important tool for biologists and sociologists who seek to understand the interactions between humans and fauna in sensitive areas. The applications relate as well to the behavioral study of human populations (hikers, mountain bikers, skiers, hunters, forest professionals ...) as animal populations (wolves, deer, wild boars, fauna carrying ticks transmitting Lyme disease ...) cf figure 1.

The goal of the WildCount project is the development of an inexpensive hunting camera for autonomus recognition and counting of the presence of humans (anonymously) and animals in to the wild and protected areas such as authorized paths of natural parks, near montain refuges, avalanche zones ... (cf figure 2). This sensor will use inexpensive off-the-shelves components but nevertheless offering good performance (image resolution, energy consumption, etc.). The technologies envisaged will be based on deep learning techniques (ie neural networks). Neural networks will be trained from the human, animal and vehicle image databases depending on the installation / observation context. The calibration will be done in partnership with the technical teams of the Zone Ateliers Alpes (including the Station Alpine Joseph Fourier and the Parc National des Ecrins).

Two proofs of concept were carried out in 2020 ([MAIx Sipeed](https://www.seeedstudio.com/sipeed) board and a LoRaWAN modem) and 2021 ([Greenwaves GAP8POC](https://greenwaves-technologies.com/gap8_mcu_ai/) boards). The new version of the WildCount edge sensor will be built around the Sony Spresense board, thermal cameras, visible-ligth cameras, PIR motion sensors, environmental sensors (temperature, humidity, pressure, etc.) and a low-power and long-range LoRaWAN communication modem (RN2483 and Lambda80) on ISM bands (868 MHz, 2.4 GHz).

This hunting camera respects the privacy of any person passing in the detection field since no image will be stored or transmitted by the sensor firmware. This hunting camera only transmits occupancy counters for the different types of species recognized by the neural network.  It can send alerts on detection according a calendar loaded on the sensor. The software architecture of the project follows the principles of edge-computing architectures: the hunting camera embeds a large part of processing (image recognition) to transmit only synthetic information to the cloud via a long range and robust communication but very low throughput network (approximately 300 baud over 1% of duty cycle). The summarized data is then stored into a datacenter for triggering alerts and displaying dashboards.

![Before Wildcount](./images/wildcount1.png)
Figure 1: before Wildcount sensor

![With Wildcount](./images/wildcount2.png)
Figure 2: with Wildcount sensor

## Architecture

![Architecture](./images/wildcount_architecture.png)

### Hunting camera BOM
* [Sony SPresense](https://developer.sony.com/develop/spresense/specifications/) main board CXD5602PWBMAIN1 (including GNSS module)
* [Sony SPresense](https://developer.sony.com/develop/spresense/specifications/) extension board (with SD card reader) CXD5602PWBEXT1
* [Sony SPresense](https://developer.sony.com/develop/spresense/specifications/) camera CXD5602PWBCAM1
* [Grove PIR sensor](https://www.seeedstudio.com/Grove-PIR-Motion-Sensor.html)
* [Melexis MLX90640](https://www.melexis.com/en/product/mlx90640/far-infrared-thermal-sensor-array) far infrared thermal camera (resolution 24x32) coming soon
* [Microchip RN2483](https://www.microchip.com/en-us/product/RN2483) LoRaWAN module
* 2x 18650 rechargeable batteries (3.7 v 3400mah/battery)
* 2x 18650 battery holders
* [Nordic Power Profiler Kit II](https://www.nordicsemi.com/Products/Development-hardware/Power-Profiler-Kit-2) for low power debugging
* [NXP Semiconductors MCU-Link Debug Probe](https://www.nxp.com/design/microcontrollers-developer-resources/mcu-link-debug-probe:MCU-LINK) for firmware debugging.

### LoRa Gateway
* [Kerlink iStation](https://www.kerlink.fr/produit/wirnet-istation/) + 5 dBi antenna

### Backend
* Chirpstack for the private LoRaWAN Network Server
* NodeRED for ETL
* InfluxDB 1.8 for time-series storage
* Grafana for dashboards
* [DeepFaune](https://www.deepfaune.cnrs.fr/) for aposteriori on-board inference checking.

## Training dataset
* 3000 photos taken by [Parc National des Ecrins](https://www.ecrins-parcnational.fr/)'s huntings cams.

## Neural networks
* [MobileNet](https://arxiv.org/abs/1704.04861)

## Firmwares
* [Arduino + Edge Impulse lib](https://gitlab.com/wildcount/firmware-spresense-arduino/)
* [Sony Spresense SDK](https://developer.sony.com/develop/spresense/docs/home_en.html) coming soon

## Test field
* [Parc National des Ecrins, Secteur Valbonnais, Entraigues](https://www.ecrins-parcnational.fr/commune/entraigues)

## Gallery

![WildCount versus Hunting Cam](./images/wildcount_huntingcam-00.jpg)
![WildCount Hunting Cam with SPresense](./images/wildcount2022_beautifulp.jpg)
![WildCount Hunting Cam with SPresense](./images/wildcount2022_openproto1.jpg)
![WildCount Hunting Cam with SPresense: camera](./images/wildcount2022_camera.jpg)
![WildCount Hunting Cam with SPresense: SD reader](./images/wildcount2022_sd.jpg)
![WildCount Hunting Cam with SPresense: test field](./images/wildcount_huntingcam-04.jpg)
![Parc National des Ecrins, secteur Valbonnais, Entraigues](./images/entraigues.jpg)
![Gateway iStation LoRa Entraigues](https://campusiot.github.io/images/kerlink-pne-entraigues.jpg)
![Fox](./images/renard_22.jpg)
![Fox](./images/renard_12.jpg)
![Boar](./images/sanglier_13.jpg)
![Roe](./images/chevreuil_35.jpg)
![Chamois](./images/chamois_25.jpg)
![Black Tetras](./images/tetras_lyre_16.jpg)
![Pyrenean Mountain Dog for protecting flock of sheep](./images/chien_de_protection_4.jpg)
![Donkey (Lynred Thermeye 80x80 thermal sensor)](images/donkey-ir.jpg)
![Dashboard](https://gitlab.com/wildcount/backend/-/raw/master/images/dashboard-01.jpg)
![Dashboard](https://gitlab.com/wildcount/backend/-/raw/master/images/dashboard-02.jpg)

## Security & Privacy
* Communications security relies on [LoRaWAN security framework](https://lora-alliance.org/wp-content/uploads/2020/11/lorawan_security_whitepaper.pdf) (AES 128)
* In production mode, pictures are not stored into the SD Card.
* In test mode, pictures are stored into the SD Card into to check and improve the neural network.

## Contact
* Didier DONSEZ, Georges QUENOT (AT univ-grenoble-alpes DOT fr)

## Partners
* [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/), [Grenoble INP](https://www.grenoble-inp.fr/), [LIG UMR 5217](https://www.liglab.fr/), [ERODS team](https://www.liglab.fr/fr/recherche/equipes-recherche/erods) and [MRIM team](https://www.liglab.fr/fr/recherche/equipes-recherche/mrim).
* [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) [MIAI](https://miai.univ-grenoble-alpes.fr/)
* [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) SAJF
* [Parc National des Ecrins](https://www.ecrins-parcnational.fr/)
* [PIA Equipex+ Terra Forma](https://terra-forma.cnrs.fr/)
* [DeepFaune](https://www.deepfaune.cnrs.fr/)
* [Zone Atelier Alpes](http://www.za-alpes.org/)

## References
* MobileNet paper: https://arxiv.org/abs/1704.04861
* TensorFlow Lite : https://www.tensorflow.org/lite?hl=fr
* MobileNet with Keras : https://keras.io/api/applications/mobilenet/
* MobileNet in TFLite: https://www.tensorflow.org/lite/guide/hosted_models
* MobileNet on GAP8 : https://github.com/GreenWaves-Technologies/image_classification_networks/tree/0ccb8b627bf1a2f5007c51bac1d3c4fa29695b17
* [Imaginecology: Awesome Deep learning for Image & Ecology](https://gitlab.com/ecostat/imaginecology)
* [DeepFaune](https://www.deepfaune.cnrs.fr/)

## Support
This work has been partially supported by [MIAI @ Grenoble Alpes (ANR-19-P3IA-0003)](https://miai.univ-grenoble-alpes.fr), [LIG Emergence](https://www.liglab.fr/en) and [PIA Terra-Forma (ANR-21-ESRE-0014)](https://terra-forma.cnrs.fr/). 

![MIAI](./images/logos/logoMIAI-rvb.png) ![LIG Lab](./images/logos/liglab-logo.png) ![PIA Terra Forma](./images/logos/terraforma-logo.png) ![Parc National des Ecrins](./images/logos/logo-pne.png)


